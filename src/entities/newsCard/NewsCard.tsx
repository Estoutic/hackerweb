import { Card, CardGrid, Div, Group, Header, Tappable, Text } from "@vkontakte/vkui";
import React from "react";
import { Link } from "react-router-dom";
import { IBaseComponentProps } from "src/shared";
import { INewsPreview } from "src/shared/news";
import "./NewsCard.css";

type Props = IBaseComponentProps & { news: INewsPreview };

const NewsCard = ({ className, news, ...rest }: Props) => {
    const { id, title, rating, date, author } = news;
    console.log(news);

    const classes = [className, "newsCard"].join(" ");

    return (
        <Group mode="plain" >
            <CardGrid size="l">
                <Card mode="outline" className={classes}>
                    <Link to={`/${id}`}>
                        <Tappable onClick={console.log} activeMode="background" hasActive>
                            <Text >{title}</Text>
                            <Div className="card_footer">
                                <Text weight="1" > {author}</Text>
                                <Text >{date}</Text>
                                <Text weight="1"> Rating: {rating}</Text>
                            </Div>
                        </Tappable>
                    </Link>
                </Card>
            </CardGrid>
        </Group>
    )
}

export default NewsCard;