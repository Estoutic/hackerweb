import { Card, CardGrid, Group, Header, Text, PanelHeader, Tappable, View } from "@vkontakte/vkui";
import React from "react";
import { NewsCard } from "../..//entities";
import { IBaseComponentProps } from "src/shared";
import "./Board.css";
import newsPreviews from "../../shared/news/consts";

type Props = IBaseComponentProps;

const Board = ({ className, ...rest }: Props) => {

    const classes = ["board", className].join(" ");

    return (
        <Group mode="plain" header={<Header mode="secondary">Новости</Header>}>
            <CardGrid size="l">
                <Card mode="shadow" className={classes}>
                    {newsPreviews.map((news) => (
                        <NewsCard news={news}></NewsCard>

                    ))}
                </Card>
            </CardGrid>
        </Group>
    )
}

export default Board;