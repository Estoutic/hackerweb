export type INewsPreview = {
    id: number;
    author: string;
    date: string;
    title: string;
    rating: number;
}