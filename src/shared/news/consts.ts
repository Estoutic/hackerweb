import { INewsPreview } from ".";

const newsPreviews: INewsPreview[] = [
    {
        id: 1,
        author: "edward",
        date: "57 minutes ago",
        title: "There's a 30-year old dead rabbit in Seven Sisters tube station (ianvisits.co.uk)",
        rating: 45
    },
    {
        id: 2,
        author: "Arteiii",
        date: "4 hours ago",
        title: "Building a Linux Container Using Namespaces: Part – 1 (2020) (polarsparc.com)",
        rating: 104
    },
    {
        id: 3,
        author: "pyinstallwoes",
        date: "5 hours ago",
        title: "DwarFS – Deduplicating Warp-Speed Advanced Read-Only File System (github.com/mhx)",
        rating: 80
    },
    {
        id: 4,
        author: "00702",
        date: "15 hours ago",
        title: "Show HN: I made a new sensor out of 3D printer filament for my PhD (paulbupejr.com)",
        rating: 686
    },
    {
        id: 5,
        author: "pyinstallwoes",
        date: "6 hours ago",
        title: "DNS over Wikipedia (github.com/aaronjanse)",
        rating: 83
    },
    {
        id: 6,
        author: "nithinbekal",
        date: "4 hours ago",
        title: "Abstract Methods and NotImplementedError in Ruby (nithinbekal.com)",
        rating: 18
    }
];

export default newsPreviews;
