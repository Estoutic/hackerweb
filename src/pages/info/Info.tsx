import { Card, Div, Text, Button } from "@vkontakte/vkui";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { IBaseComponentProps } from "src/shared/types";
import { Board } from "../../widgets";

type Props = IBaseComponentProps;

const Info = ({ className, news, ...rest }: Props) => {

    const params = useParams();
    console.log(params);


    const classes = ["info", className].join(" ")
    const { id, title, date, author, comments } = news;
    const [showComments, setShowComments] = useState(false);

    const toggleComments = () => {
        setShowComments(!showComments);
    };

    const renderComments = () => {
        if (!showComments) return null;
        return comments.map((comment) => <div key={comment.id}> {comment}</div>);
    };

    return (
        <Card mode="outline">
            <Text>{title}</Text>
            <Div>
                <Text>{author}</Text>
                <Text>{date}</Text>
                <Button onClick={toggleComments}>{showComments ? "Hide Comments" : "Show Comments"}</Button>
                <Button>Refresh Comments</Button>
                <Button>Back to News</Button>
            </Div>
            {renderComments()}
        </Card>
    );
};

export default Info;