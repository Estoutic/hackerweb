import React from "react";
import { IBaseComponentProps } from "src/shared/types";
import { Board } from "../../widgets";

type Props = IBaseComponentProps;

const Home = ({className, ...rest} : Props) => {

    const classes = ["home", className].join(" ")
    return ( 
        <div className={classes}>
            <Board></Board>
        </div>
    )
}

export default Home;