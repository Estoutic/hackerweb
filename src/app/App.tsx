import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Home, Info } from "../pages";

import '@vkontakte/vkui/dist/vkui.css';
import "./App.css"

function App() {
  const queryClient = new QueryClient();

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Home />,
      errorElement: <h1>Error</h1>,
    },
    {
      path: "/:id",
      element: <Info />,
    },
    // {
    //   path: "/login",
    //   element: <AuthPage />
    // },
  ]);

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <RouterProvider router={router} />
      </QueryClientProvider>
    </>
  );
}

export default App;
